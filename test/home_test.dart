import 'package:flutter_test/flutter_test.dart';
import 'package:mymovie/app/data/provider/service_provider.dart';

class sampelMovie {
  int id = 744746;
}

void main() {
  test("test data hasil request api error atau tidak", () async {
    var upcoming = await ServiceProvider().getUpcoming();
    var popular = await ServiceProvider().getPopular();
    var genre = await ServiceProvider().getGenre();
    var detail = await ServiceProvider().getDeteail(sampelMovie().id);
    var trailer = await ServiceProvider().getVideo(sampelMovie().id);
    expect(popular.statusCode, equals(200));
    expect(upcoming.statusCode, equals(200));
    expect(genre.statusCode, equals(200));
    expect(detail.statusCode, equals(200));
    expect(trailer.statusCode, equals(200));
  });
}
