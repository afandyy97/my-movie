import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mymovie/app/bindings/bindings.dart';
import 'package:mymovie/app/routes/app_page.dart';
import 'package:get/get.dart';
import 'package:mymovie/app/ui/pages/home.dart';
import 'package:mymovie/app/ui/pages/test.dart';

void main() async {
  await GetStorage.init();
  runApp(MyApp());
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  AllBindings().dependencies();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
      builder: (context, child) {
        return MediaQuery(
          child: child!,
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
      getPages: Appage.pages,
    );
  }
}
