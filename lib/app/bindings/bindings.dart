import 'package:get/get.dart';
import 'package:mymovie/app/controllers/detail_controller.dart';
import 'package:mymovie/app/controllers/home_controller.dart';
import 'package:mymovie/app/controllers/network_check.dart';
import 'package:mymovie/app/ui/pages/detail.dart';

class AllBindings implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController(), fenix: true);
    Get.lazyPut<DetailController>(() => DetailController(), fenix: true);
    Get.put(NetworkCheck());
  }
}
