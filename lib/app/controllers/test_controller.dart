import 'package:get/get.dart';
import 'package:mymovie/app/data/models/videos_models.dart';
import 'package:mymovie/app/data/provider/service_provider.dart';

class TestController extends GetxController {
  late VideoModels video;

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();

    try {
      var response = await ServiceProvider().getVideo(660353);
      video = VideoModels.fromJson(response.body);
    } catch (e) {
      print(e);
    }
  }
}
