import 'package:get/get.dart';

import 'package:mymovie/app/data/models/dataMovie_models.dart';
import 'package:mymovie/app/data/models/detail_models.dart';
import 'package:mymovie/app/data/models/videos_models.dart';
import 'package:mymovie/app/data/provider/service_provider.dart';

import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class DetailController extends GetxController {
  var id = Get.arguments;
  DetailMovie? detailMovie;
  late DataMovie similarMovie;
  late VideoModels video;
  late var key = ''.obs;
  late YoutubePlayerController controller;

  var isLoading = true.obs;

  @override
  void onInit() async {
    super.onInit();

    requestData();
  }

  requestData() async {
    try {
      var response = await ServiceProvider().getDeteail(id);
      var responseVideo = await ServiceProvider().getVideo(id);
      var responseSimilar = await ServiceProvider().getSimilar(id);

      if (response.statusCode == 200) {
        detailMovie = DetailMovie.fromJson(response.body);

        similarMovie = DataMovie.fromJson(responseSimilar.body);
        video = VideoModels.fromJson(responseVideo.body);
        if (video.results.isNotEmpty) {
          controller = YoutubePlayerController(
            initialVideoId: video.results[0].key,
            params: YoutubePlayerParams(
              showControls: true,
              showFullscreenButton: true,
            ),
          );
        }

        isLoading.value = false;
      }
    } catch (e) {
      print(e);
    }
  }
}
