import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mymovie/app/data/models/dataMovie_models.dart';
import 'package:mymovie/app/data/models/genre_models.dart';

import 'package:mymovie/app/data/provider/service_provider.dart';

class HomeController extends GetxController {
  final box = GetStorage();
  DataMovie? movieUpcoming;
  GenreMovie? genreMovie;
  DataMovie? popular;
  var isnew = true.obs;
  var isLoading = true.obs;
  var currentIdx = 0.obs;

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
    final cached = box.read('home');
    if (cached != null) {
      isLoading.value = false;

      movieUpcoming = DataMovie.fromJson(cached['upcoming']);
      popular = DataMovie.fromJson(cached['popular']);
      genreMovie = GenreMovie.fromJson(cached['genre']);
    } else {
      requestData();
    }
  }

  requestData() async {
    try {
      var responseUpcoming = await ServiceProvider().getUpcoming();
      var responseGenre = await ServiceProvider().getGenre();
      var responsePopular = await ServiceProvider().getPopular();

      if (responseUpcoming.statusCode == 200 &&
          responseGenre.statusCode == 200 &&
          responsePopular.statusCode == 200) {
        movieUpcoming = DataMovie.fromJson(responseUpcoming.body);

        genreMovie = GenreMovie.fromJson(responseGenre.body);
        popular = DataMovie.fromJson(responsePopular.body);
        if (box.read('home') == null) {
          box.write('home', {
            "upcoming": movieUpcoming!,
            "popular": popular!,
            "genre": genreMovie!,
          });
        } else {
          box.remove('home');
          box.write('home', {
            "upcoming": movieUpcoming!,
            "popular": popular!,
            "genre": genreMovie!,
          });
        }
        isLoading.value = false;
      }
    } catch (e) {
      print(e);
    }
  }
}
