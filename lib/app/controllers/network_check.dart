import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mymovie/app/controllers/home_controller.dart';

class NetworkCheck extends GetxController {
  final c = Get.find<HomeController>();
  var connectionStatus = true.obs;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> connectivitySubscription;

  @override
  void onInit() {
    super.onInit();

    initConnectivity();
    connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    result = await _connectivity.checkConnectivity();

    try {} on PlatformException catch (e) {
      print(e);
    }

    return _updateConnectionStatus(result);
  }

  _updateConnectionStatus(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.wifi:
        // connectionStatus.isTrue;
        c.requestData();
        break;
      case ConnectivityResult.mobile:
        // connectionStatus.isTrue;
        c.requestData();
        break;
      case ConnectivityResult.ethernet:
        // connectionStatus.isTrue;
        c.requestData();
        break;
      case ConnectivityResult.none:
        connectionStatus.isFalse;
        dialog();
        break;
      default:
        connectionStatus.isFalse;
        dialog();
    }
  }

  @override
  void onClose() {
    super.onClose();
    connectivitySubscription.cancel();
  }

  void dialog() {
    Get.defaultDialog(
      barrierDismissible: false,
      title: "Network Error",
      contentPadding: const EdgeInsets.symmetric(horizontal: 15),
      cancel: const ButtonCancel(),
      radius: 5,
      content: const Text(
        'Failed Connect to the Internet. Please, check your internet connection!',
        textAlign: TextAlign.center,
      ),
    );
  }
}

class ButtonCancel extends StatelessWidget {
  const ButtonCancel({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (() {
        Get.back();
      }),
      child: Container(
        margin: const EdgeInsets.only(bottom: 10),
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: const Color.fromRGBO(21, 101, 192, 1),
        ),
        child: const Text(
          'Close',
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}
