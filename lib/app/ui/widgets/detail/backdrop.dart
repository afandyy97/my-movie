import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mymovie/app/controllers/detail_controller.dart';
import 'package:mymovie/app/data/models/api_models.dart';
import 'package:mymovie/app/ui/widgets/global/shimmer_effect.dart';

class Backdrop extends StatelessWidget {
  const Backdrop({
    Key? key,
    required this.c,
  }) : super(key: key);

  final DetailController c;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (rect) {
        return const LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Colors.black, Colors.transparent],
        ).createShader(Rect.fromLTRB(0, 0, rect.width, rect.height));
      },
      blendMode: BlendMode.dstIn,
      child: c.isLoading.value == true
          ? const ShimmerEffect(
              h: 300,
              w: double.infinity,
              op: 1,
            )
          : CachedNetworkImage(
              imageUrl: ApiData.baseimageURL + c.detailMovie!.backdropPath,
              fit: BoxFit.cover,
            ),
    );
  }
}
