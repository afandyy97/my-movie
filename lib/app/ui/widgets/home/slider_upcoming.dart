import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:mymovie/app/controllers/home_controller.dart';
import 'package:mymovie/app/data/models/api_models.dart';

class SliderUpcoming extends StatelessWidget {
  const SliderUpcoming({
    Key? key,
    required this.c,
  }) : super(key: key);

  final HomeController c;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CarouselSlider(
            options: CarouselOptions(
              onPageChanged: (index, reason) {
                c.currentIdx.value = index;
              },
              height: 430,
              aspectRatio: 1,
              viewportFraction: .75,
              initialPage: 0,
              enableInfiniteScroll: true,
              reverse: false,
              autoPlay: true,
              autoPlayInterval: const Duration(seconds: 10),
              autoPlayAnimationDuration: const Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastOutSlowIn,
              enlargeCenterPage: true,
              scrollDirection: Axis.horizontal,
            ),
            items: c.movieUpcoming!.results.map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(
                      left: 5.0,
                      right: 5.0,
                      bottom: 25,
                      top: 7,
                    ),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.4),
                          spreadRadius: 3,
                          blurRadius: 8,
                          offset:
                              const Offset(0, 12), // changes position of shadow
                        ),
                      ],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: CachedNetworkImage(
                        imageUrl: ApiData.baseimageURL + i.posterPath,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) => const Center(
                          child: SpinKitCircle(
                            color: Colors.amber,
                          ),
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
              );
            }).toList(),
          ),

          // caption
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              children: [
                Text(
                  c.movieUpcoming!.results[c.currentIdx.value].title.toString(),
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontWeight: FontWeight.w800,
                    letterSpacing: 1.2,
                  ),
                ),
                const SizedBox(
                  height: 2,
                ),

                // list genre
                Wrap(
                  alignment: WrapAlignment.center,
                  children: c.movieUpcoming!.results[c.currentIdx.value]
                          .genreIds.isEmpty
                      ? []
                      : c.movieUpcoming!.results[c.currentIdx.value].genreIds
                          .asMap()
                          .entries
                          .map((e) {
                          var separator = e.key >=
                                  c.movieUpcoming!.results[c.currentIdx.value]
                                          .genreIds.length -
                                      1
                              ? ""
                              : " / ";
                          return Text(
                            c
                                    .genreMovie!
                                    .genres[c.genreMovie!.genres.indexWhere(
                                        (element) =>
                                            element.id ==
                                            c
                                                .movieUpcoming!
                                                .results[c.currentIdx.value]
                                                .genreIds[e.key])]
                                    .name +
                                separator,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.w300),
                          );
                        }).toList(),
                ),

                const SizedBox(
                  height: 5,
                ),

                Wrap(
                  alignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    // rating
                    Text(
                      'Rating : ${c.movieUpcoming!.results[c.currentIdx.value].voteAverage.toString()}',
                      style: const TextStyle(
                          fontSize: 13,
                          color: Colors.white,
                          fontWeight: FontWeight.w300),
                    ),

                    const Text(
                      ' | ',
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),

                    const Text(
                      ' | ',
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),

                    Text(
                      'Language : ${c.movieUpcoming!.results[c.currentIdx.value].originalLanguage.toString()}',
                      style: const TextStyle(
                          fontSize: 13,
                          color: Colors.white,
                          fontWeight: FontWeight.w300),
                    ),

                    const Text(
                      ' | ',
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),

                    Text(
                      '${c.movieUpcoming!.results[c.currentIdx.value].releaseDate}',
                      // '${DateFormat.yMMMMd('en_US').format(DateTime.parse(c.movieUpcoming!.results[c.currentIdx.value].releaseDate)).toString()}',
                      style: const TextStyle(
                          fontSize: 13,
                          color: Colors.white,
                          fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
