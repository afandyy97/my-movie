import 'package:flutter/material.dart';

class TitleCategories extends StatelessWidget {
  const TitleCategories({
    Key? key,
    required this.title,
    required this.link,
  }) : super(key: key);

  final String title;
  final link;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style: const TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.w600),
          ),
        ),
        IconButton(
          onPressed: link,
          icon: const Icon(
            Icons.arrow_forward_ios,
            color: Colors.white,
            size: 16,
            shadows: [
              BoxShadow(
                color: Color.fromRGBO(21, 101, 192, 1),
                spreadRadius: 5,
                blurRadius: 5,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
