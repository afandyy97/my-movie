import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerEffect extends StatelessWidget {
  const ShimmerEffect({
    Key? key,
    required this.h,
    required this.w,
    required this.op,
  }) : super(key: key);

  final double h;
  final double w;
  final double op;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        child: Container(
          height: h,
          width: w,
          color: Colors.grey,
        ),
        baseColor: Colors.grey[500]!.withOpacity(op),
        highlightColor: Colors.grey[100]!.withOpacity(op));
  }
}
