import 'package:flutter/cupertino.dart';

class HorizontalLine extends StatelessWidget {
  const HorizontalLine({
    Key? key,
    required this.padding,
    required this.col,
  }) : super(key: key);

  final double padding;
  final Color col;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: padding),
      height: 2,
      color: col,
    );
  }
}
