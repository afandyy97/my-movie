import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:mymovie/app/data/models/api_models.dart';
import 'package:get/get.dart';
import 'package:mymovie/app/routes/routname.dart';

class SliderPopular extends StatelessWidget {
  const SliderPopular({
    Key? key,
    required this.c,
  }) : super(key: key);

  final c;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      height: 200,
      child: ListView.builder(
        padding: const EdgeInsets.all(2),
        primary: false,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: c.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () => Get.toNamed(
              RoutName.detail,
              arguments: c[index].id,
            ),
            child: Container(
              margin: EdgeInsets.only(
                  right: 15, bottom: 5, left: index == 0 ? 15 : 0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 2,
                      offset: const Offset(0, 4)),
                ],
              ),
              child: Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: CachedNetworkImage(
                      imageUrl: ApiData.baseimageURL + c[index].posterPath,
                      progressIndicatorBuilder:
                          (context, url, downloadProgress) =>
                              const SpinKitCircle(
                        color: Colors.amber,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Colors.black,
                          Colors.black.withOpacity(0.8),
                          Colors.black.withOpacity(0.8),
                          Colors.black.withOpacity(0.5),
                          Colors.transparent,
                        ],
                      ),
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(5),
                      ),
                    ),
                    child: Row(children: [
                      const Icon(
                        Icons.star,
                        size: 10,
                        color: Colors.orange,
                      ),
                      const SizedBox(
                        width: 3,
                      ),
                      Text(
                        c[index].voteAverage.toString(),
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: 10,
                            fontWeight: FontWeight.w700),
                      ),
                    ]),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
