import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:mymovie/app/controllers/detail_controller.dart';
import 'package:mymovie/app/ui/widgets/detail/backdrop.dart';
import 'package:mymovie/app/ui/widgets/detail/rating_duration_lang.dart';
import 'package:mymovie/app/ui/widgets/global/horizontalLine.dart';
import 'package:mymovie/app/ui/widgets/global/shimmer_effect.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class Detail extends StatelessWidget {
  final c = Get.find<DetailController>();

  @override
  Widget build(BuildContext context) {
    final Size s = MediaQuery.of(context).size;
    return Scaffold(
      body: Obx(() => SingleChildScrollView(
            scrollDirection: Axis.vertical,
            primary: true,
            child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: s.height),
              child: Container(
                padding: const EdgeInsets.only(bottom: 20),
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color.fromRGBO(13, 71, 161, 1),
                      Color.fromRGBO(66, 165, 245, 1),
                      Color.fromRGBO(21, 101, 192, 1),
                    ],
                  ),
                ),
                child: Column(
                  children: [
                    // backdrop
                    SizedBox(
                      height: 300,
                      child: Backdrop(c: c),
                    ),

                    // detail caption
                    Column(
                      children: [
                        // title

                        c.isLoading.isTrue
                            ? const ShimmerEffect(h: 35, w: 200, op: .3)
                            : Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 30),
                                width: s.width,
                                child: Text(
                                  c.detailMovie!.title,
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                      fontSize: 30,
                                      shadows: [
                                        BoxShadow(
                                            color:
                                                Color.fromARGB(255, 8, 58, 116),
                                            spreadRadius: 15,
                                            blurRadius: 15,
                                            offset: Offset(3, 2)),
                                      ],
                                      color: Colors.white,
                                      fontWeight: FontWeight.w900),
                                ),
                              ),

                        const SizedBox(height: 5),

                        // genre
                        c.isLoading.isTrue
                            ? const ShimmerEffect(h: 20, w: 250, op: .3)
                            : Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 30),
                                width: s.width,
                                child: // list genre
                                    Wrap(
                                  alignment: WrapAlignment.center,
                                  children: c.detailMovie!.genres.isEmpty
                                      ? []
                                      : c.detailMovie!.genres
                                          .asMap()
                                          .entries
                                          .map((e) {
                                          var separator = e.key >=
                                                  c.detailMovie!.genres.length -
                                                      1
                                              ? ""
                                              : " / ";
                                          return Text(
                                            c.detailMovie!.genres[e.key].name +
                                                separator,
                                            overflow: TextOverflow.ellipsis,
                                            style: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 13,
                                                fontStyle: FontStyle.italic,
                                                fontWeight: FontWeight.w300),
                                          );
                                        }).toList(),
                                ),
                              ),

                        const SizedBox(height: 5),

                        // release date
                        c.isLoading.isTrue
                            ? const ShimmerEffect(h: 25, w: 150, op: .3)
                            : Text(
                                'Release date : ${DateFormat.yMMMMd('en_US').format(DateTime.parse(c.detailMovie!.releaseDate)).toString()}',
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w300),
                              ),

                        const SizedBox(height: 15),

                        // rating, duration, language
                        c.isLoading.isTrue
                            ? const ShimmerEffect(h: 60, w: 300, op: .3)
                            : Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                width: s.width,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    RatingDurationLang(
                                        text1: c.detailMovie!.voteAverage
                                            .toString(),
                                        text2: "IMDB"),
                                    RatingDurationLang(
                                        text1:
                                            c.detailMovie!.runtime.toString(),
                                        text2: "Minutes"),
                                    RatingDurationLang(
                                        text1: c.detailMovie!.originalLanguage,
                                        text2: "Language"),
                                  ],
                                ),
                              ),

                        const SizedBox(height: 10),

                        // border
                        const HorizontalLine(
                            padding: 40, col: Color.fromRGBO(25, 118, 210, .5)),

                        const SizedBox(height: 10),

                        // trailer
                        c.isLoading.isTrue
                            ? const ShimmerEffect(h: 150, w: 300, op: .3)
                            : Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 40),
                                child: c.video.results.isEmpty
                                    ? const SizedBox(height: 10)
                                    : VideoPlayer(c: c)),

                        const SizedBox(height: 15),

                        // Synopsis

                        c.isLoading.isTrue
                            ? Column(children: const [
                                ShimmerEffect(h: 20, w: 250, op: .3),
                                SizedBox(height: 3),
                                ShimmerEffect(h: 20, w: 200, op: .3),
                                SizedBox(height: 3),
                                ShimmerEffect(h: 20, w: 150, op: .3),
                                SizedBox(height: 3),
                              ])
                            : Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Text(c.detailMovie!.overview,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      fontSize: 13,
                                      color: Colors.white,
                                    )),
                              ),

                        const SizedBox(
                          height: 20,
                        ),

                        // border
                        const HorizontalLine(
                            padding: 0, col: Color.fromARGB(125, 27, 83, 140)),

                        // SliderPopular(c: c.similarMovie.results)
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )),
      floatingActionButton: Container(
        alignment: Alignment.topRight,
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 50),
        child: IconButton(
          onPressed: () => Get.back(),
          icon: const Icon(
            Icons.close,
            size: 25,
            color: Colors.white,
            shadows: [
              BoxShadow(
                  color: Color.fromARGB(255, 8, 58, 116),
                  spreadRadius: 15,
                  blurRadius: 5,
                  offset: Offset(3, 2)),
            ],
          ),
        ),
      ),
    );
  }
}

class VideoPlayer extends StatelessWidget {
  const VideoPlayer({
    Key? key,
    required this.c,
  }) : super(key: key);

  final DetailController c;

  @override
  Widget build(BuildContext context) {
    return YoutubePlayerControllerProvider(
      // Provides controller to all the widget below it.
      controller: c.controller,
      child: YoutubePlayerIFrame(
        aspectRatio: 16 / 9,
      ),
    );
    ;
  }
}
