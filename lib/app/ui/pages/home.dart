import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:mymovie/app/controllers/home_controller.dart';
import 'package:mymovie/app/controllers/network_check.dart';
import 'package:mymovie/app/routes/routname.dart';
import 'package:mymovie/app/ui/widgets/global/slider_popular.dart';
import 'package:mymovie/app/ui/widgets/home/header.dart';
import 'package:mymovie/app/ui/widgets/home/slider_upcoming.dart';
import 'package:mymovie/app/ui/widgets/home/title_categories.dart';

class Home extends StatelessWidget {
  final c = Get.find<HomeController>();
  final network = Get.find<NetworkCheck>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 59, 147, 220),
      body: Container(
        padding: const EdgeInsets.only(top: 40),
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Color.fromRGBO(66, 165, 245, 1),
              Color.fromRGBO(21, 101, 192, 1),
              Color.fromRGBO(13, 71, 161, 1),
            ],
          ),
        ),
        child: LiquidPullToRefresh(
          onRefresh: () async {
            await c.requestData();
          },
          color: Colors.blue[100],
          height: 70,
          backgroundColor: const Color.fromARGB(255, 59, 147, 220),
          showChildOpacityTransition: false,
          springAnimationDurationInMilliseconds: 400,
          child: Column(
            children: [
              // Header
              const Header(),

              Obx(
                () => c.isLoading.isFalse
                    ? Expanded(
                        child: SingleChildScrollView(
                          primary: true,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              const Padding(
                                padding: EdgeInsets.only(top: 15),
                                child: TitleCategories(
                                    title: 'Upcoming Movie', link: null),
                              ),

                              const SizedBox(
                                height: 10,
                              ),

                              // slider upcoming movies
                              InkWell(
                                onTap: () => Get.toNamed(RoutName.detail,
                                    arguments: c.movieUpcoming!
                                        .results[c.currentIdx.value].id),
                                child: SliderUpcoming(c: c),
                              ),

                              const SizedBox(
                                height: 10,
                              ),

                              Container(
                                width: double.infinity,
                                height: 1,
                                color: const Color.fromRGBO(25, 118, 210, 1),
                              ),

                              const TitleCategories(
                                  title: 'Popular', link: null),

                              // slider
                              SliderPopular(c: c.popular!.results)
                            ],
                          ),
                        ),
                      )
                    : const SpinKitCircle(
                        color: Colors.amber,
                      ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
