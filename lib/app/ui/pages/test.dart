import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:mymovie/app/controllers/network_check.dart';
import 'package:mymovie/app/controllers/test_controller.dart';

class TestConnection extends StatelessWidget {
  // final c = Get.find<NetworkCheck>();
  final c = Get.put(TestController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(),
    );
  }
}
