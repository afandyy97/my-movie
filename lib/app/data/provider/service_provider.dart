import 'package:get/get.dart';
import 'package:mymovie/app/data/models/api_models.dart';

class ServiceProvider extends GetConnect {
  // Get request
  Future<Response> getUpcoming() =>
      get('${ApiData.baseURL}movie/upcoming${ApiData.apiKey}');
  Future<Response> getUpcoming2() =>
      get('${ApiData.baseURL}movie/upcoming${ApiData.apiKey}&page=2');
  Future<Response> getUpcoming3() =>
      get('${ApiData.baseURL}movie/upcoming${ApiData.apiKey}&page=3');
  Future<Response> getGenre() =>
      get('${ApiData.baseURL}genre/movie/list${ApiData.apiKey}');
  Future<Response> getPopular() =>
      get('${ApiData.baseURL}movie/popular${ApiData.apiKey}');
  Future<Response> getDeteail(int id) async => await Future.delayed(
      const Duration(seconds: 1),
      () => get('${ApiData.baseURL}/movie/$id${ApiData.apiKey}'));
  Future<Response> getTopRated() =>
      get('${ApiData.baseURL}movie/top_rated${ApiData.apiKey}');
  Future<Response> getVideo(int id) =>
      get('${ApiData.baseURL}/movie/$id/videos${ApiData.apiKey}');
  Future<Response> getSimilar(int id) =>
      get('${ApiData.baseURL}/movie/$id/similar${ApiData.apiKey}');
}
