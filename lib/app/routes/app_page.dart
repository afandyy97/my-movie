import 'package:get/get.dart';
import 'package:mymovie/app/routes/routname.dart';
import 'package:mymovie/app/ui/pages/detail.dart';
import 'package:mymovie/app/ui/pages/home.dart';

class Appage {
  static final pages = [
    GetPage(
      name: RoutName.home,
      page: () => Home(),
    ),
    GetPage(
      name: RoutName.detail,
      page: () => Detail(),
    ),
  ];
}
